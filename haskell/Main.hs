{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))

import Model.GameInit
import Model.CarPositions
import Model.GameEnd
import Message
import State

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  handleMessages h initialState

handleMessages h s = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success (Unknown t) -> putStrLn ("Unknown message: " ++ t) >> handleMessages h s
        Success serverMessage -> handleServerMessage h serverMessage s
        Error s -> fail $ "Error decoding message " ++ (\(Success a) -> fst a) ((fromJSON json) :: Result (String, Value)) ++ ": " ++ s ++ "\n" ++ msg
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

handleServerMessage :: Handle -> ServerMessage -> PlayState -> IO ()
handleServerMessage h serverMessage s = do
  (response,sp) <- react serverMessage s
  --forM_ responses $ hPutStrLn h
  hPutStrLn h response
  handleMessages h sp

{-
respond :: ServerMessage -> GameState -> IO [ClientMessage]
respond Join = do
  putStrLn "Joined"
  return [pingMessage]
respond (GameInit gameInit) = do
  putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
  return [pingMessage]
respond (CarPositions carPositions) = do
  return $ [throttleMessage 0.5]
respond (Unknown msgType) = do
  putStrLn $ "Unknown message: " ++ msgType
  return [pingMessage]
respond TournamentEnd = do
  putStrLn "Tournament Ended!"
  return [pingMessage]
-}
{-
respond message = case message of
  Join -> do
    putStrLn "Joined"
    return [pingMessage]
  GameInit gameInit -> do
    putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    return [pingMessage]
  CarPositions carPositions -> do
    return $ [throttleMessage 1.0]
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return [pingMessage]
-}

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join"          = Success Join
  | msgType == "yourCar"       = YourCar <$> (fromJSON msgData)
  | msgType == "gameInit"      = GameInit <$> (fromJSON msgData)
  | msgType == "gameStart"     = Success GameStart
  | msgType == "carPositions"  = CarPositions <$> (fromJSON msgData)
  | msgType == "gameEnd"       = Success GameEnd --GameEnd <$> (fromJSON msgData)
  | msgType == "tournamentEnd" = Success TournamentEnd
  | msgType == "crash"         = Crash <$> (fromJSON msgData)
  | msgType == "spawn"         = Spawn <$> (fromJSON msgData)
  | msgType == "lapFinished"   = LapFinished <$> (fromJSON msgData)
  | msgType == "dnf"           = DNF <$> (fromJSON msgData)
  | msgType == "finish"        = Finish <$> (fromJSON msgData)
  | otherwise = Success $ Unknown msgType

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
