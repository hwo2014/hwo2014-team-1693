{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module Model.GameEnd where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

import Model.GameInit

-- ResultValue

data ResultInfo = ResultInfo {
  laps   :: Maybe Int,
  ticks  :: Maybe Int,
  millis :: Maybe Int
} deriving (Show)

instance FromJSON ResultInfo where
  parseJSON (Object v) =
    ResultInfo <$>
    (v .:? "laps") <*>
    (v .:? "ticks") <*>
    (v .:? "millis")

-- Result

data ResultValue = ResultValue {
  car    :: Car,
  result :: ResultInfo
} deriving (Show)

instance FromJSON ResultValue where
  parseJSON (Object v) =
    ResultValue <$>
    (v .: "car") <*>
    (v .: "result")

-- GameEndData

data GameEndData = GameEndData {
  results  :: [ResultValue],
  bestLaps :: [ResultValue]
} deriving (Show)

instance FromJSON GameEndData where
  parseJSON (Object v) =
    GameEndData <$>
    (v .: "results") <*>
    (v .: "bestLaps")

-- Helpers
