{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module Model.LapFinished where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))

import Model.GameInit

-- LapTime

data LapTime = LapTime {
  lap   :: Int,
  lticks  :: Int,
  lmillis :: Int
} deriving (Show)

instance FromJSON LapTime where
  parseJSON (Object v) =
    LapTime <$>
    (v .: "lap") <*>
    (v .: "ticks") <*>
    (v .: "millis")

-- RaceTime

data RaceTime = RaceTime {
  laps   :: Int,
  rticks  :: Int,
  rmillis :: Int
} deriving (Show)

instance FromJSON RaceTime where
  parseJSON (Object v) =
    RaceTime <$>
    (v .: "laps") <*>
    (v .: "ticks") <*>
    (v .: "millis")

-- Ranking

data Ranking = Ranking {
  overall    :: Int,
  fastestLap :: Int
} deriving (Show)

instance FromJSON Ranking where
  parseJSON (Object v) =
    Ranking <$>
    (v .: "overall") <*>
    (v .: "fastestLap")

-- LapFinishedData

data LapFinishedData = LapFinishedData {
  car      :: CarId,
  lapTime  :: LapTime,
  raceTime :: RaceTime,
  ranking  :: Ranking
} deriving (Show)

instance FromJSON LapFinishedData where
  parseJSON (Object v) =
    LapFinishedData <$>
    (v .: "car") <*>
    (v .: "lapTime") <*>
    (v .: "raceTime") <*>
    (v .: "ranking")

-- Helpers
