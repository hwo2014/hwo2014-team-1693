module Message
( ClientMessage
, ServerMessage(..)
, joinMessage
, throttleMessage
, pingMessage
) where

import Model.GameInit
import Model.CarPositions
import Model.GameEnd
import Model.LapFinished

type ClientMessage = String

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

data ServerMessage = Join
                   | YourCar CarId
                   | GameInit GameInitData
                   | GameStart
                   | CarPositions [CarPosition]
                   | GameEnd --GameEndData
                   | TournamentEnd
                   | Crash CarId
                   | Spawn CarId
                   | LapFinished LapFinishedData
                   | DNF CarId
                   | Finish CarId
                   | Unknown String
                   deriving (Show)
