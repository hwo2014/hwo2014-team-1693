module State where

import Control.Monad (when)

import Model.CarPositions
import Model.GameInit
import Model.GameEnd
import Model.LapFinished
import Message


-- Join -> YourCar -> GameInit -> [GameStart -> [CarPositions] -> GameEnd] -> TournamentEnd
-- Crash / Spawn / LapFinished / DNF / Finish


data PlayState = PreTournament
               | Joined
               | PreGame CarId
               | Initialized CarId Track [Car]
               | Started CarId Track [Car]
               | Running CarId Track [CarPosition]
               | Crashed CarId Track [CarPosition]
               | Finished CarId Track [CarPosition]
               | PostGame --GameEndData
               | PostTournament
               deriving (Show)

initialState :: PlayState
initialState = PreTournament


react :: ServerMessage -> PlayState -> IO (ClientMessage,PlayState)
-- Join
react Join PreTournament = do
  putStrLn "Joined"
  return (pingMessage,Joined)
-- YourCar
react (YourCar cid) Joined = do
  return (pingMessage,PreGame cid)
-- GameInit
react (GameInit gameInit@(GameInitData (Race track cars _))) (PreGame cid) = do
  putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
  return (pingMessage,Initialized cid track cars)
-- GameStart
react GameStart s = do
  putStrLn "Game Started!"
  return (pingMessage,s)
-- CarPositions
react (CarPositions ps) (Initialized cid track cars) = react (CarPositions ps) (Running cid track ps)
react (CarPositions ps) (Running cid track psp) = return (throttleMessage 0.5,Running cid track ps)
-- GameEnd
react (GameEnd) (Running cid track ps) = do
  putStrLn "Game Ended!"
  return (pingMessage,PostGame)
-- TournamentEnd
react TournamentEnd (PostGame) = do
  putStrLn "Tournament Ended!"
  return (pingMessage,PostTournament)
-- Crash
react (Crash _) s@(Running _ _ _) = return (pingMessage,s)
-- Spawn
react (Spawn _) s@(Running _ _ _) = return (pingMessage,s)
-- LapFinished
react (LapFinished l) s@(Running cid _ _) = do
  when (Model.LapFinished.car l == cid) $ putStrLn "Finished a lap"
  return (pingMessage,s)
-- DNF
react (DNF _) s@(Running _ _ _) = return (pingMessage,s)
-- Finish
react (Finish cidf) s@(Running cid _ _) = do
  when (cidf == cid) (putStrLn "You've finished the game!")
  return (pingMessage,s)
-- ERROR
react a b = error $ "Invalid message/state combination!\n" ++ show a ++ "\n" ++ show b
